---
title: "Mineral Ridge"
date: 2023-01-21T00:00:00+00:00
images: ["images/IMG_0414.jpg"]
locations: "Mineral Ridge"
seasons: ["Winter"]
# youtubes: ["khuDWBwxc5g", "OOz5meOnAns", "mgmxbve1dGs"]
---

Quick evening hike of Mineral Ridge!
